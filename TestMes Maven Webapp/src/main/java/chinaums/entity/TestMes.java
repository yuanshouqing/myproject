package chinaums.entity;

import java.io.Serializable;

public class TestMes implements Serializable{
	private static final long serialVersionUID = 1L;
    private String CHANNEL_ID;
    private String ENCRYPT_CARD_NO;
    private String ENCRYPT_PHONE;
    private String ENCRYPT_NAME;
    private String ENCRYPT_CERT_NO;
    private Integer rownum;
	
	public String getCHANNEL_ID() {
		return CHANNEL_ID;
	}
	public void setCHANNEL_ID(String cHANNEL_ID) {
		CHANNEL_ID = cHANNEL_ID;
	}
	public String getENCRYPT_CARD_NO() {
		return ENCRYPT_CARD_NO;
	}
	public void setENCRYPT_CARD_NO(String eNCRYPT_CARD_NO) {
		ENCRYPT_CARD_NO = eNCRYPT_CARD_NO;
	}
	public String getENCRYPT_PHONE() {
		return ENCRYPT_PHONE;
	}
	public void setENCRYPT_PHONE(String eNCRYPT_PHONE) {
		ENCRYPT_PHONE = eNCRYPT_PHONE;
	}
	public String getENCRYPT_NAME() {
		return ENCRYPT_NAME;
	}
	public void setENCRYPT_NAME(String eNCRYPT_NAME) {
		ENCRYPT_NAME = eNCRYPT_NAME;
	}
	public String getENCRYPT_CERT_NO() {
		return ENCRYPT_CERT_NO;
	}
	public void setENCRYPT_CERT_NO(String eNCRYPT_CERT_NO) {
		ENCRYPT_CERT_NO = eNCRYPT_CERT_NO;
	}
	public Integer getRownum() {
		return rownum;
	}
	public void setRownum(Integer rownum) {
		this.rownum = rownum;
	}
    
}
