package chinaums.entity;

import java.io.Serializable;

public class Test_Configure implements Serializable{
	private static final long serialVersionUID = 1L;
    private Integer ID;
    private String ORGSENDERID;
    private String TRANSID;
    private String PKCS;
	public Integer getID() {
		return ID;
	}
	public void setID(Integer iD) {
		ID = iD;
	}
	public String getORGSENDERID() {
		return ORGSENDERID;
	}
	public void setORGSENDERID(String oRGSENDERID) {
		ORGSENDERID = oRGSENDERID;
	}
	public String getTRANSID() {
		return TRANSID;
	}
	public void setTRANSID(String tRANSID) {
		TRANSID = tRANSID;
	}
	public String getPKCS() {
		return PKCS;
	}
	public void setPKCS(String pKCS) {
		PKCS = pKCS;
	}
    
}
