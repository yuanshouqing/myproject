package chinaums.service;

import java.util.List;
import java.util.Map;

public interface MessageService {

    public List<Map<String,Object>> getData();
}
