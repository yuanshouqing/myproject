package chinaums.service.impl;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import chinaums.dao.TestMesDao;
import chinaums.service.TestMesService;
import chinaums.service.Test_ConfigureService;
@Service
public class TestMesServiceImpl implements TestMesService{
	private static Logger log= LoggerFactory.getLogger(TestMesServiceImpl.class);
    @Autowired
    private TestMesDao dao;
    @Autowired
    private Test_ConfigureService testService;
    private String POST_URL="http://172.30.252.10:9001/api/smau";
    private String SMAU_PUB_KEY="30819f300d06092a864886f70d010101050003818d0030818902818100a136bd1399f94e46ff42eb6fad480621ca3e03c01e9fc8e1eab6d75dabc63e47ef1c2064fb10abaa491b9452e2d504de88223775e14931c6d8c7aab0f13d6673f567d7d0b0769539a749d920f50d7421f3fd66b5425bc3dee6f27b82de6243894e79b76a478efe79ff44ee813e789150871498186cbf1672a306469d250b703d0203010001";
    private String CONTENT_TYPE_TEXT = "application/json";
    //渠道的私钥,这里是编号为10008的渠道的私钥
  	//private static String CHANNEL_PRIVATE_KEY=null;
	@Override
	public String test() {
		List<Map<String, Object>> mesMap = dao.selectAll();
		String msgCode="";
		if(mesMap==null || mesMap.size()<=0){
			msgCode="没有值";
			log.info(msgCode);
			return msgCode;
		}
		
		//数量
		int listSize = mesMap.size();
		//线程个数
		int RunSize=10;
		//定义数量
		int count=0;
		//判断集合数是否大于线程数，小于则线程为1，count等于集合大小
		if(listSize<RunSize){
			RunSize=1;
			count=listSize;
		}else{
			//每个线程执行的数量
			count=listSize/RunSize;
		}
		//创建线程池
		ExecutorService fixedThreadPool=Executors.newFixedThreadPool(RunSize);
		//创建线程辅助类
		CountDownLatch countDownLatch = new CountDownLatch(RunSize);
		List<Map<String, Object>> mapList=new ArrayList<Map<String,Object>>();
		//分割数据，避免线程拿到重复数据，将数据分页
		for (int j = 0; j < RunSize; j++) {
		    if((j+1)==RunSize){
		        int startIndex = (j*count);
		        int endIndex = mesMap.size();
		        mapList =mesMap.subList(startIndex,endIndex);
		    }else{
		        int startIndex = j*count;
		        int endIndex = (j+1)*count;
		        mapList =mesMap.subList(startIndex,endIndex);
		    }
		    //创建一个线程类，将数据分割，然后每个线程执行相应的任务
		    TestMesRunnable urlrun=new TestMesRunnable(mapList,testService,countDownLatch,POST_URL,SMAU_PUB_KEY,CONTENT_TYPE_TEXT);
		    //执行线程
		    fixedThreadPool.execute(urlrun);
		}
		try {
			//等待所有线程执行完
			countDownLatch.await();
			//关闭线程
			fixedThreadPool.shutdown();
		} catch (InterruptedException e) {
			e.printStackTrace();
		}finally{
			msgCode="ok";
		}
		return msgCode;
	}
	@Override
	public List<Map<String, Object>> selectAll() {
		return dao.selectAll();
	}
}
