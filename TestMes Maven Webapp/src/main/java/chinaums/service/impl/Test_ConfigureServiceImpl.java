package chinaums.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import chinaums.dao.Test_ConfigureDao;
import chinaums.entity.Test_Configure;
import chinaums.service.Test_ConfigureService;
@Service
public class Test_ConfigureServiceImpl implements Test_ConfigureService{
    @Autowired
	private Test_ConfigureDao dao;
	@Override
	public Test_Configure selectConfigureByChannleId(String OrgSenderID) {
		return dao.selectConfigureByChannleId(OrgSenderID);
	}

}
