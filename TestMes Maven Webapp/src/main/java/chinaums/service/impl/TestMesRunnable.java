package chinaums.service.impl;

import java.io.IOException;
import java.nio.charset.UnsupportedCharsetException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.CountDownLatch;

import org.apache.http.ParseException;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.util.EntityUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import chinaums.common.util.PackAndUnpackDemo;
import chinaums.common.util.des;
import chinaums.entity.Test_Configure;
import chinaums.service.Test_ConfigureService;

public class TestMesRunnable implements Runnable{
	private static Logger log= LoggerFactory.getLogger(TestMesRunnable.class);
	private Test_ConfigureService testService;
	private List<Map<String, Object>> mapList;
	private CountDownLatch countDownLatch;
	private String POST_URL="http://172.30.252.10:9001/api/smau";
    private String SMAU_PUB_KEY="30819f300d06092a864886f70d010101050003818d0030818902818100a136bd1399f94e46ff42eb6fad480621ca3e03c01e9fc8e1eab6d75dabc63e47ef1c2064fb10abaa491b9452e2d504de88223775e14931c6d8c7aab0f13d6673f567d7d0b0769539a749d920f50d7421f3fd66b5425bc3dee6f27b82de6243894e79b76a478efe79ff44ee813e789150871498186cbf1672a306469d250b703d0203010001";
    private String CONTENT_TYPE_TEXT = "application/json";
    public TestMesRunnable(List<Map<String, Object>> mapList,Test_ConfigureService testService,CountDownLatch countDownLatch,String POST_URL,String SMAU_PUB_KEY,String CONTENT_TYPE_TEXT){
    	this.testService=testService;
    	this.mapList=mapList;
    	this.countDownLatch=countDownLatch;
    	this.POST_URL=POST_URL;
    	this.SMAU_PUB_KEY=SMAU_PUB_KEY;
    	this.CONTENT_TYPE_TEXT=CONTENT_TYPE_TEXT;
    }
	@Override
	public void run() {
		try {
			List<Map<String, Object>> newList=new ArrayList<Map<String,Object>>();
			//获取集合大小
			int getotherList=mapList.size();
			//定义count数量
			int getCount=200;
			//定义循环次数
			int getTimeCount=0;
			//当小于count数量则只循环一次，count等于集合大小
			if(getotherList<=getCount){
				getTimeCount=1;
				getCount=getotherList;
			}else{
				getTimeCount=(getotherList/getCount)+1;
			}
			//将数据进行分页
			for (int pageNum = 1; pageNum < getTimeCount + 1; pageNum++)
			{
				int starNum = (pageNum - 1) * getCount;
				int endNum = pageNum * getCount > getotherList ? (getotherList) : pageNum * getCount;
				newList =mapList.subList(starNum,endNum);
				for (Map<String, Object> map : newList) {
					String channelid=null;
					String CHANNEL_PRIVATE_KEY = null;
					Map<String, String> body = null;
					try {
						channelid = map.get("CHANNEL_ID")==null?"":map.get("CHANNEL_ID").toString();
						String encrypt_name=map.get("ENCRYPT_NAME")==null?"":map.get("ENCRYPT_NAME").toString();
						String newEncrypt_name=des.decrypt(encrypt_name);
						String encrypt_card_no=map.get("ENCRYPT_CARD_NO")==null?"":map.get("ENCRYPT_CARD_NO").toString();
						String newEncrypt_card_no=des.decrypt(encrypt_card_no);
						String encrypt_phone=map.get("ENCRYPT_PHONE")==null?"":map.get("ENCRYPT_PHONE").toString();
						String newEncrypt_phone=des.decrypt(encrypt_phone);
						String encrypt_cert_no=map.get("ENCRYPT_CERT_NO")==null?"":map.get("ENCRYPT_CERT_NO").toString();
						String newEncrypt_cert_no=des.decrypt(encrypt_cert_no);
						String cert_type=map.get("CERT_TYPE")==null?"":map.get("CERT_TYPE").toString();
						Test_Configure keyMap = testService.selectConfigureByChannleId(channelid);
						if(keyMap==null||"".equals(keyMap)){
							continue;
						}
						CHANNEL_PRIVATE_KEY = keyMap.getPKCS();
						body = new HashMap<String, String>();
						body.put("cardNo", newEncrypt_card_no);
						body.put("certNo", newEncrypt_cert_no);
						body.put("certType", cert_type);
						body.put("name", newEncrypt_name);	
						body.put("phoneNo", newEncrypt_phone);
					} catch (Exception e2) {
						// TODO Auto-generated catch block
						e2.printStackTrace();
					}
					String msg=null;
					try {
						msg = PackAndUnpackDemo.getRequestMsg(body, CHANNEL_PRIVATE_KEY, channelid, "00011", "银行卡验证");
					} catch (Exception e1) {
						e1.printStackTrace();
					}
					
					CloseableHttpClient httpclient = HttpClients.custom().build();
			        try {
			        	
			            HttpPost httpPost = new HttpPost(POST_URL);
			            
			            System.out.println("Executing request " + httpPost.getRequestLine());
			            
			    		StringEntity se = new StringEntity(msg, "UTF-8");
			    		se.setContentType(CONTENT_TYPE_TEXT);
			    		se.setContentEncoding( "UTF-8");
			    		httpPost.setEntity(se);
			    	//	System.out.println("test"+httpPost.toString());
			    		for(int i=0;i<1;i++){
			            CloseableHttpResponse response = null;
						try {
							response = httpclient.execute(httpPost);
						} catch (ClientProtocolException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						} catch (IOException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
			            try {
			                //HttpEntity entity = response.getEntity();
			                System.out.println(response.getStatusLine());
			                log.info("getStatusLine========"+response.getStatusLine());
			                if(200==(response.getStatusLine().getStatusCode())){
			                	String respStr = null;
								try {
									respStr = EntityUtils.toString(response.getEntity(),"UTF-8");
								} catch (ParseException e) {
									// TODO Auto-generated catch block
									e.printStackTrace();
								} catch (IOException e) {
									// TODO Auto-generated catch block
									e.printStackTrace();
								}
				                //String respStr=IOUtils.toString(entity.getContent());
				                //进行报文签名的验证
				                System.out.println(respStr);
				                log.info("respStr========"+respStr);
				                boolean result = false;
								try {
									result = PackAndUnpackDemo.verify(respStr, SMAU_PUB_KEY);
								} catch (Exception e) {
									// TODO Auto-generated catch block
									e.printStackTrace();
								}
				                System.out.println("结果报文签名验证结果为:"+result);
				                log.info("result========"+result);
			                }
			            } finally {
			                try {
								response.close();
							} catch (IOException e) {
								// TODO Auto-generated catch block
								e.printStackTrace();
							}
			            }
			    		}
			        } finally {
			            try {
							httpclient.close();
						} catch (IOException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
			        }
				}
			}
		} catch (UnsupportedCharsetException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}finally{
			//关闭辅助类线程
			countDownLatch.countDown();
		}
		
	}

}
