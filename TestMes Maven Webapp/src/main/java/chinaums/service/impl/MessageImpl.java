package chinaums.service.impl;

import chinaums.dao.MessageDao;
import chinaums.service.MessageService;
import com.alibaba.fastjson.JSON;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;

@Service
public class MessageImpl  implements MessageService {
//ctrl+o  提示需要重写的方法
    @Autowired
    private MessageDao messageDao;

    @Override
    public List<Map<String, Object>> getData() {
       List<Map<String,Object>>  list =messageDao.getData();
        return list;
    }





}
