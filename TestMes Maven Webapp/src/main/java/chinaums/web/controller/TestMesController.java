package chinaums.web.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import chinaums.service.TestMesService;

@Controller
@RequestMapping("test")
public class TestMesController {
@Autowired
private TestMesService testService;


@RequestMapping("MyJsp")
public String Myjsp(){
	
	return "MyJsp";
}

@RequestMapping("testMes")
@ResponseBody
public String runTest(){
	String result=null;
	for (int i = 0; i < 100; i++) {
		result=testService.test();
	}
	return result;
}
}
