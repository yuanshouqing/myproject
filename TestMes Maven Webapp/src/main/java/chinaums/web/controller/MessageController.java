package chinaums.web.controller;

import chinaums.service.MessageService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.List;
import java.util.Map;


@Controller
@ResponseBody
public class MessageController {

    @Autowired
    private MessageService   messageService;


    @RequestMapping("/getData")
    public List<Map<String,Object>>    getData(){

        List<Map<String,Object>> list=messageService.getData();
        System.out.println(list);
        return   list;
    }

}
