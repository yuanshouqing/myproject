package chinaums.web.exception;

public class UserException extends RuntimeException {

    public UserException() {
        super("非法用户!");
    }

    public UserException(String message) {
        super(message);
    }

    public UserException(String message, Throwable cause) {
        super(message, cause);
    }

    public UserException(Throwable cause) {
        super(cause);
    }

}
