package chinaums.web.exception;

import sun.tools.tree.SuperExpression;

public class TokenException extends RuntimeException {

    private static final long serialVersionUID = 1L;

    public TokenException() {
        super("客户端Token失效!");
    }

//    JDK1.6下不存在该方法
//    public TokenException(String message, Throwable cause,
//                          boolean enableSuppression, boolean writableStackTrace) {
//        super(message, cause, enableSuppression, writableStackTrace);
//    }

    public TokenException(String message, Throwable cause) {
        super(message, cause);
    }

    public TokenException(String message) {
        super(message);
    }

    public TokenException(Throwable cause) {
        super(cause);
    }
}
