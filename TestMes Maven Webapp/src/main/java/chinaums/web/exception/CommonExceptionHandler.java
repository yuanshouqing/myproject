package chinaums.web.exception;

import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;

import chinaums.common.util.Result;
import chinaums.web.exception.TokenException;

import javax.servlet.http.HttpServletRequest;


@ControllerAdvice
public class CommonExceptionHandler {


    @ExceptionHandler(TokenException.class)
    @ResponseBody
    public Result handleBizExp(HttpServletRequest request, Exception ex) {

        return new Result().failure(ex.getMessage());
    }


    @ExceptionHandler(UserException.class)
    @ResponseBody
    public Result handleUserException(HttpServletRequest request, Exception ex) {

        return new Result().failure(ex.getMessage());
    }
}
