package  chinaums.common.test;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLConnection;
import java.security.KeyFactory;
import java.security.MessageDigest;
import java.security.PrivateKey;
import java.security.PublicKey;
import java.security.Signature;
import java.security.spec.PKCS8EncodedKeySpec;
import java.security.spec.X509EncodedKeySpec;
import java.util.List;

import org.bouncycastle.util.encoders.UrlBase64;

import net.sf.json.JSONObject;

public class TestAdvSms {
	
	private final static String PRI_KEY = "30820277020100300d06092a864886f70d0101010500048202613082025d02010002818100a63c8a428726ac9f4399491db6469a5b6cab007bd16ace16c5d40a9afe2094d79f61965f551e537df6c4e74df10362409e3a5d3492cbbd8a2824e421c0854296bb0dbc75ee1c71f38b0a070791211a612ca370548ef0391740060104ab9b1253cf2c14c4d402d0acf0921a9d85b82dfcf008af22ded0279dfdf39ffc4fbe70ab02030100010281810099713639b460996424d47fade24b073dae6fa571a4dace422cf6cd97454d33b658128ee78e3d00da3b8682eee9b1b2e66c66f913871072f2ec9d28ca92d72e9f4d309c95897103594c1dba891d4cd559a779ef9985aa4ce82880cf06cd8592ad2513e1322fb09f78de53c77ce02f0ad9e1779829567cd7ba539770966cee47c9024100ea7bdd39e91a14ad9f714b2177d756310a58debf01d0d21bdf18a5141ee0fdfc40fa68a4589c2eb8eb42cf1c99fd9c152a1933a3ca17a7c5ac3dbcf23d0cf8c7024100b57d83690126d2b0a139a625c5c6b295e3fdb6827f86be22c8c72c29c78fb23a0b12aa03cfb9f87c22e125e118a35406e4a8329d7129544a24a222c7dce2ccfd02410090ab0f18959ac3bb432df0fe9b23be47cf6c79313b7b8cc62964d9248ad0a1a32aabd437e17baacfe84675e5dd943ce249f0f68d4d383f0894761ac711a7992d02407279fafbaec848393558e19e0dc4293d3694001d0c4777830555ec5b660f13825d1da6b3c4c12f254df2ea2bb398c2159f0120fc0236e97de2e18a42bae72c19024002611da3fc65fe20303a9a6710a858f8b81801d879333de5d6d9d30b40ef889422f5f18f4e77fdc8d0f8c2d4f0769f451305dde6a36901e675c102bcb566c05d";
	private final static String PUB_KEY = "30819f300d06092a864886f70d010101050003818d0030818902818100a1d63a7f19a91759572dd2e5d00e0e22fa5f7cc18fbcca99a21225701c1ebe275744c4d5a4cfbe624c65423780d06974c3f4fa5b60cf3590bfeaba9f80a8abc04c98ee5809c5fd2d09f307f63b358549e5d0835da11decec7be57d578416994c6b118e2b4a3ee015d7b11bfe771275f71a395dd9dfc248c2f3c8f03f7081b5c30203010001";
	
	private final static String url = "http://210.22.91.77:7018/adv-sms/sms/gen/genUrls";
	
	
	public static void main(String []args) throws Exception {
		//System.out.println("1111111111111"+url.substring(0, 37));

		String channel = "106101", feedbackUrl = "https://apply.gzcb.com.cn/index.html", count = "2", currentTime = "20170615162600";
		
		// 私钥签名
		String signData = String.format("channel=%s&feedbackUrl=%s&count=%s&currentTime=%s", channel, feedbackUrl, count, currentTime);
		byte []sign = sign(signData.getBytes(), ByteUtils.hexString2ByteArray(PRI_KEY));
		String token = new String(UrlBase64.encode(sign));

		String param = signData + "&token=" + token;
		
		String rspResult = doHttpPost(url,param, "UTF-8");
		
		JSONObject rspJson = JSONObject.fromObject(rspResult);
		
		
		String rspCode = rspJson.getString("rspCode");
		String rspMsg = rspJson.getString("rspMsg");
		List ids = (List<String>)rspJson.get("ids");
		token = rspJson.getString("token");
		
		String signResult = null;
		StringBuilder sb = new StringBuilder();
		for(int i = 0; i < ids.size(); i++) {
			sb.append(ids.get(i));
			if(i+1 < ids.size()) {
				sb.append(",");				
			}
		}
		
		boolean verfy = verify(sb.toString().getBytes(), ByteUtils.hexString2ByteArray(PUB_KEY),
				UrlBase64.decode(token.getBytes("UTF-8")));		
		
		System.out.println(verfy);
		
	}

	
	public static String doHttpPost(String url, String content,String chartset) {
		HttpURLConnection connection = null;
		try {
			long start = System.currentTimeMillis();
			System.out.println("request:"+url);
			 System.out.println("请求报文:\n"+content);
			connection = (HttpURLConnection) new URL(url).openConnection();
			connection.setDoOutput(true);
			connection.setDoInput(true);
			connection.setRequestMethod("POST");
			connection.setUseCaches(false);
			connection.setInstanceFollowRedirects(true);
			connection.setRequestProperty("Accept-Charset", chartset);

			// connection.setRequestProperty("Content-Type",
			// "application/json");
			connection.connect();
			OutputStream out = connection.getOutputStream();
			out.write(content.getBytes(chartset));

			out.flush();
			out.close();

			String response = getResponseText(connection,chartset);
			 System.out.println("响应报文:\n"+response);
			 long end = System.currentTimeMillis();
			 System.out.println("doHttpPost cost time: " + (end-start) + " ms");
			return response;
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			connection.disconnect();
		}
		return null;
	}
	
	private static String getResponseText(URLConnection connection,String chartset) {
		BufferedReader reader = null;
		StringBuilder sbd = null;
		try {
			reader = new BufferedReader(new InputStreamReader(
					connection.getInputStream(),chartset));
			String line;
			sbd = new StringBuilder();
			while ((line = reader.readLine()) != null) {
				sbd.append(line).append("\n");
			}
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			if (reader != null) {
				try {
					reader.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}
		if (sbd == null)
			return null;

		return sbd.toString();
	}

	/**
	 * 私钥签名
	 * 
	 * @param data
	 *            待签名数据
	 * @param priKey
	 *            签名私钥 PKCS8Encoded
	 * @return 签名结果
	 * @throws Exception
	 */
	public static byte[] sign(byte[] data, byte[] priKey) {
		byte[] sign=null;
		try{
			PKCS8EncodedKeySpec privateKeySpec = new PKCS8EncodedKeySpec(priKey);
			KeyFactory keyFactory = KeyFactory.getInstance("RSA");
			PrivateKey privateKey = keyFactory.generatePrivate(privateKeySpec);
	
			//对数据进行SHA-256签名
			MessageDigest md= MessageDigest.getInstance("SHA-256");
			byte[] sha256Digest=md.digest(data);
			
			Signature instance = Signature.getInstance("SHA1withRSA");
			instance.initSign(privateKey);
			instance.update(sha256Digest);
			sign = instance.sign();
		}catch(Exception e){
			e.printStackTrace();
			throw new RuntimeException(e);
		}
		return sign;
	}

	/**
	 * 验证签名
	 * 
	 * @param data
	 *            待验签数据
	 * @param pubkey
	 *            公钥
	 * @param sign
	 *            签名
	 * @return
	 * @throws Exception
	 */
	public static boolean verify(byte[] data, byte[] pubkey, byte[] sign){
		boolean result=false;
		try{
			// 获取公钥对象
			X509EncodedKeySpec bobPubKeySpec = new X509EncodedKeySpec(pubkey);
			KeyFactory keyFactory = java.security.KeyFactory.getInstance("RSA");
			PublicKey pubKey = keyFactory.generatePublic(bobPubKeySpec);

			//对数据进行SHA-256签名
			MessageDigest md= MessageDigest.getInstance("SHA-256");
			byte[] sha256Digest=md.digest(data);
			//System.out.println("sha256Digest"+Hex.encodeHexString(sha256Digest));
			//System.out.println("sign"+Hex.encodeHexString(sign));
			// 验证签名
			Signature instance = Signature.getInstance("SHA1withRSA");
			instance.initVerify(pubKey);
			instance.update(sha256Digest);
			result = instance.verify(sign);
		}catch(Exception e){
			throw new RuntimeException(e);
		}
		return result;
	}
}
