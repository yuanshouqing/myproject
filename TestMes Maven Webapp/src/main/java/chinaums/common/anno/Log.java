package chinaums.common.anno;

import java.lang.annotation.*;

/**
 * 
 * @author Kingsley
 *@date  2017年4月11日 下午9:59:51
 */
@Target(ElementType.METHOD)
@Retention(RetentionPolicy.RUNTIME)
@Documented
public @interface Log {

    String value() default "";
}
