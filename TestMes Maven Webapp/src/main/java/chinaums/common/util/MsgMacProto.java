package chinaums.common.util;

import com.alibaba.fastjson.annotation.JSONField;

/**
 * 报文签名实体类
 * 
 * @author wuwenzhe
 * 
 */
public class MsgMacProto {
	/**
	 * 签名字段
	 */
	@JSONField(serialize = false)
	private String signature;

	@JSONField(name = "Signature")
	public String getSignature() {
		return signature;
	}

	@JSONField(name = "Signature")
	public void setSignature(String signature) {
		this.signature = signature;
	}
}
