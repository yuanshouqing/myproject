package chinaums.common.util;

import java.security.Key;
import java.security.SecureRandom;

import javax.crypto.Cipher;
import javax.crypto.SecretKeyFactory;
import javax.crypto.spec.DESKeySpec;

//import org.apache.commons.lang.StringUtils;

public class des {

	private static final String key = "J59gJ2a89912Ajt7uS5ch2kKhR0T5Yo8Fc2SAI17tI7S1w85P7NuOZPkNM9s42";

	/**
	 * des加密
	 * */
	public static String encrypt(String message) {
		try {
			byte[] data = message.getBytes("UTF-8");
			DESKeySpec dks = new DESKeySpec(key.getBytes());
			SecureRandom sr = new SecureRandom();
			SecretKeyFactory keyFactory = SecretKeyFactory.getInstance("DES");
			Key secretKey = keyFactory.generateSecret(dks);
			Cipher cipher = Cipher.getInstance("DES");
			cipher.init(1, secretKey, sr);
			byte[] bytes = cipher.doFinal(data);
			String resultString = new String(Base64Util.encode(bytes));
			return resultString;
		} catch (Exception e) {
			// logger.error("敏感信息加密异常", e);
			throw new RuntimeException();
		}

	}

	/**
	 * des解密
	 * */
	public static String decrypt(String encrpytedMessage) {
		try {
			byte[] data = Base64Util.decode(encrpytedMessage.toCharArray());
			SecureRandom sr = new SecureRandom();
			DESKeySpec dks = new DESKeySpec(key.getBytes());
			SecretKeyFactory keyFactory = SecretKeyFactory.getInstance("DES");

			Key secretKey = keyFactory.generateSecret(dks);
			Cipher cipher = Cipher.getInstance("DES");
			cipher.init(2, secretKey, sr);
			return new String(cipher.doFinal(data), "UTF-8");
		} catch (Exception e) {
			// logger.error("敏感信息解密异常", e);
		}
		throw new RuntimeException();
	}

	public static void main(String[] args) throws Exception {
		String s = "Axt+TzqHq5M=";
		String p = decrypt(s);
		System.out.println(p);
	}

}
