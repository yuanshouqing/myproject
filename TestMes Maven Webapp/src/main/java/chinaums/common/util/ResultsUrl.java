package chinaums.common.util;

import java.io.Serializable;
import java.util.List;

public class ResultsUrl implements Serializable {
	private static final long serialVersionUID = 2061590310189800664L;
	private List<Results> urls;

	public List<Results> getUrls() {
		return urls;
	}

	public void setUrls(List<Results> urls) {
		this.urls = urls;
	}
	
}
