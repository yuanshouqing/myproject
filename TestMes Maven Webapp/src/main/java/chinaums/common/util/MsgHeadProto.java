package chinaums.common.util;

import com.alibaba.fastjson.annotation.JSONField;

/**
 * 报文头实体类，格式按照《银联商务内部系统接口规范V0.4》
 * 
 * @author wuwenzhe
 * 
 */
public class MsgHeadProto {
	/**
	 * 版本号
	 */
	public static final String VERSION_ID = "001";

	/**
	 * 字符编码:UTF-8
	 */
	public static final String CHARSET_UTF8 = "UTF-8";

	/**
	 * 字符编码:GBK
	 */
	public static final String CHARSET_GBK = "GBK";

	/**
	 * 交易类型:涉及钱的交易
	 */
	public static final String TRANS_TYPE_BANKING = "01";

	/**
	 * 交易类型:涉及钱的撤销交易
	 */
	public static final String TRANS_TYPE_REVOKE = "02";

	/**
	 * 交易类型:不涉及钱的信息类报文
	 */
	public static final String TRANS_TYPE_INFO = "03";

	/**
	 * 报文总记录数
	 */
	public static final String MSG_TOTAL_NUM = "1";

	/**
	 * 交易方向:请求
	 */
	public static final String TYPE_REQUEST = "0";
	/**
	 * 交易方向:响应
	 */
	public static final String TYPE_RESPONSE = "1";
	/**
	 * 透传标志:透传
	 */
	public static final String TRANSPARENT_FLAG_GET_THROUGH = "1";
	/**
	 * 透传标志：非透传
	 */
	public static final String TRANSPARENT_FLAG_DONOT_GET_THROUGH = "0";

	/**
	 * 透传报文类型：XML
	 */
	public static final String TRANSPARENT_TYPE_XML = "XML";

	/**
	 * 透传报文类型:8583
	 */
	public static final String TRANSPARENT_TYPE_8583 = "8583";

	/**
	 * 透传报文类型:JSON
	 */
	public static final String TRANSPARENT_TYPE_JSON = "JSON";
	
	public static final String TRANSPARENT_TYPE_NONE="NONE";

	/**
	 * 版本号 请求和响应中都必填
	 */
	private String versionID;

	/**
	 * 字符编码 请求和响应中都必填
	 */
	private String charSet;

	/**
	 * 请求方系统代码 XX(总分平台)XXXX(地区代码)XXXX(序号)，系统中用作渠道ID 请求和响应中都必填
	 */
	private String orgSenderID;

	/**
	 * 接受方系统代码 000000XXXX,后四位需要申请 请求和响应中都必填
	 */
	private String orgReceiverID;

	/**
	 * 交易类型 请求和响应中都必填
	 */
	private String transType;

	/**
	 * 交易代码，当前系统填写ServiceID，长度小于15 请求和响应中都必填
	 */
	private String transID;

	/**
	 * 交易名称,当前系统填写Service名称 请求和响应中都必填
	 */
	private String transName;

	/**
	 * 请求方交易日期:yyyyMMdd 请求和响应中都必填
	 */
	private String orgSendDate;

	/**
	 * 请求方交易时间:HHmmss请求和响应中都必填
	 */
	private String orgSendTime;

	/**
	 * 请求方交易流水号，有请求方保证一天之内不重复 请求和响应中都必填
	 */
	private String orgSendTransID;

	/**
	 * 接受方交易日期:yyyyMMdd 响应中必填
	 */
	private String orgRcvDate;

	/**
	 * 接受方交易时间:HHmmssSSS 响应中必填
	 */
	private String orgRcvTime;

	/**
	 * 接受方交易流水号，yyyyMMdd+sequence 响应中必填
	 */
	private String orgRcvTransID;

	/**
	 * 报文总记录数 请求和响应中都必填
	 */
	private String msgTotalNum;

	/**
	 * 报文当前笔数 请求和响应中都必填
	 */
	private String msgNum;

	/**
	 * 接收方处理结果码:20XXXXXX 响应中必填
	 */
	private String msgRspCode;

	/**
	 * 接受方处理结果说明(小于100个字符) 响应中必填
	 */
	private String msgRspCodeResults;

	/**
	 * 交易方向 请求和响应中都必填
	 */
	private String type;

	/**
	 * 透传标志 请求和响应中都必填
	 */
	private String transParentFlag;

	/**
	 * 透传报文类型 请求和响应中都必填
	 */
	private String transParentType;

	/**
	 * 加密证书编码 请求和响应中都必填
	 */
	private String encryptCertId;

	/**
	 * 保留字段 请求和响应均可以不填写
	 */
	private String reserve;

	@JSONField(name = "VersionID", ordinal = 0)
	public String getVersionID() {
		return versionID;
	}

	@JSONField(name = "VersionID")
	public void setVersionID(String versionID) {
		this.versionID = versionID;
	}

	@JSONField(name = "CharSet", ordinal = 1)
	public String getCharSet() {
		return this.charSet;
	}

	@JSONField(name = "CharSet")
	public void setCharSet(String charSet) {
		this.charSet = charSet;
	}

	@JSONField(name = "OrgSenderID", ordinal = 2)
	public String getOrgSenderID() {
		return this.orgSenderID;
	}

	@JSONField(name = "OrgSenderID")
	public void setOrgSenderID(String orgSenderID) {
		this.orgSenderID = orgSenderID;
	}

	@JSONField(name = "OrgReceiverID", ordinal = 3)
	public String getOrgReceiverID() {
		return this.orgReceiverID;
	}

	@JSONField(name = "OrgReceiverID")
	public void setOrgReceiverID(String orgReceiverID) {
		this.orgReceiverID = orgReceiverID;
	}

	@JSONField(name = "TransType", ordinal = 4)
	public String getTransType() {
		return this.transType;
	}

	@JSONField(name = "TransType")
	public void setTransType(String transType) {
		this.transType = transType;
	}

	@JSONField(name = "TransID", ordinal = 5)
	public String getTransID() {
		return this.transID;
	}

	@JSONField(name = "TransID")
	public void setTransID(String transID) {
		this.transID = transID;
	}

	@JSONField(name = "TransName", ordinal = 6)
	public String getTransName() {
		return this.transName;
	}

	@JSONField(name = "TransName")
	public void setTransName(String transName) {
		this.transName = transName;
	}

	@JSONField(name = "OrgSendDate", ordinal = 7)
	public String getOrgSendDate() {
		return this.orgSendDate;
	}

	@JSONField(name = "OrgSendDate")
	public void setOrgSendDate(String orgSendDate) {
		this.orgSendDate = orgSendDate;
	}

	@JSONField(name = "OrgSendTime", ordinal = 8)
	public String getOrgSendTime() {
		return this.orgSendTime;
	}

	@JSONField(name = "OrgSendTime")
	public void setOrgSendTime(String orgSendTime) {
		this.orgSendTime = orgSendTime;
	}

	@JSONField(name = "OrgSendTransID", ordinal = 9)
	public String getOrgSendTransID() {
		return this.orgSendTransID;
	}

	@JSONField(name = "OrgSendTransID")
	public void setOrgSendTransID(String orgSendTransID) {
		this.orgSendTransID = orgSendTransID;
	}

	@JSONField(name = "OrgRcvDate", ordinal = 10)
	public String getOrgRcvDate() {
		return this.orgRcvDate;
	}

	@JSONField(name = "OrgRcvDate")
	public void setOrgRcvDate(String orgRcvDate) {
		this.orgRcvDate = orgRcvDate;
	}

	@JSONField(name = "OrgRcvTime", ordinal = 11)
	public String getOrgRcvTime() {
		return this.orgRcvTime;
	}

	@JSONField(name = "OrgRcvTime")
	public void setOrgRcvTime(String orgRcvTime) {
		this.orgRcvTime = orgRcvTime;
	}

	@JSONField(name = "OrgRcvTransID", ordinal = 12)
	public String getOrgRcvTransID() {
		return this.orgRcvTransID;
	}

	@JSONField(name = "OrgRcvTransID")
	public void setOrgRcvTransID(String orgRcvTransID) {
		this.orgRcvTransID = orgRcvTransID;
	}

	@JSONField(name = "MsgTotalNum", ordinal = 13)
	public String getMsgTotalNum() {
		return this.msgTotalNum;
	}

	@JSONField(name = "MsgTotalNum")
	public void setMsgTotalNum(String msgTotalNum) {
		this.msgTotalNum = msgTotalNum;
	}

	@JSONField(name = "MsgNum", ordinal = 14)
	public String getMsgNum() {
		return this.msgNum;
	}

	@JSONField(name = "MsgNum")
	public void setMsgNum(String msgNum) {
		this.msgNum = msgNum;
	}

	@JSONField(name = "MsgRspCode", ordinal = 15)
	public String getMsgRspCode() {
		return this.msgRspCode;
	}

	@JSONField(name = "MsgRspCode")
	public void setMsgRspCode(String msgRspCode) {
		this.msgRspCode = msgRspCode;
	}

	@JSONField(name = "MsgRspCodeResults", ordinal = 16)
	public String getMsgRspCodeResults() {
		return this.msgRspCodeResults;
	}

	@JSONField(name = "MsgRspCodeResults")
	public void setMsgRspCodeResults(String msgRspCodeResults) {
		this.msgRspCodeResults = msgRspCodeResults;
	}

	@JSONField(name = "Type", ordinal = 17)
	public String getType() {
		return this.type;
	}

	@JSONField(name = "Type")
	public void setType(String type) {
		this.type = type;
	}

	@JSONField(name = "TransParentFlag", ordinal = 18)
	public String getTransParentFlag() {
		return this.transParentFlag;
	}

	@JSONField(name = "TransParentFlag")
	public void setTransParentFlag(String transParentFlag) {
		this.transParentFlag = transParentFlag;
	}

	@JSONField(name = "TransParentType", ordinal = 19)
	public String getTransParentType() {
		return this.transParentType;
	}

	@JSONField(name = "TransParentType")
	public void setTransParentType(String transParentType) {
		this.transParentType = transParentType;
	}

	@JSONField(name = "EncryptCertId", ordinal = 20)
	public String getEncryptCertId() {
		return this.encryptCertId;
	}

	@JSONField(name = "EncryptCertId")
	public void setEncryptCertId(String encryptCertId) {
		this.encryptCertId = encryptCertId;
	}

	@JSONField(name = "Reserve", ordinal = 21)
	public String getReserve() {
		return this.reserve;
	}

	@JSONField(name = "Reserve")
	public void setReserve(String reserve) {
		this.reserve = reserve;
	}

//	 public static void main(String[] args){
//	 MsgHead header=new MsgHead();
//	 header.setCharSet(CHARSET_GBK);
//	 String str=JSON.toJSONString(header,SerializerFeature.WriteMapNullValue);
//	 MsgHead head=JSON.parseObject(str, MsgHead.class);
//	
//	 }
}
