package chinaums.common.util;

import java.util.HashMap;
import java.util.Map;

import org.apache.commons.io.IOUtils;
import org.apache.http.HttpEntity;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;


/**
 * 发送Http Post请求
 * @author wuwenzhe
 *
 */
public class HttpRequestDemo {
	private static final String POST_URL="http://172.30.252.10:9001/api/service"; //测试环境
	//private static final String POST_URL="http://localhost:8080/api/service"; //本地环境
	//渠道的私钥,这里是编号为10008的渠道的私钥
	private static final String CHANNEL_PRIVATE_KEY="30820276020100300d06092a864886f70d0101010500048202603082025c0201000281810091c997463138fcb1f3611fc38d39878e665865ffc1fbbfe0bb03eb315a1896f3d98f874d72bb98b49631addcc2e884d9ff09e0299ccfc4ebc7cef52f2059d86fc9bd9a4a92a45969342d84821efeffd58e80045fe3db98e3a8f65aa238fa170890714fdcf00d2d60841c1261c56753c2958f4f6603d4d3e23db5cc42d6a2cbcf020301000102818022b80f2224c3cd0e360759d0b0d7d40c1a1c9041fdf90de39fb619b359a10486133866c10f8354a52125b8a19bfb52f435942d01cbfa010c23bc0a510c53c942ce1142b6aaaff71677b7e6d22ca92b39773addc978393d5ccf28666c6c409c989333db698fd3733f77e534ef56190e4410180a0589d9c20f54cb8abc8ba76cc5024100c4b1d8941555169774b9ba2fd4abc504c8c8ecfbb3afe4737c8896644229b5e32a4f3c8eb2d355b8bebcefb0d5c8280d4c906d49dd5a816cc1586f8bfc04cb8b024100bdbe66043f1e2c22cda8adda9826136e644365a00a4d6b229b3d60d486dbe9af3f6ecdc6fd505c94dd6a24f004dd568431f4f3204b43730ea319d9782213194d02410091315ad70cdc2f7e038ae9d64eddb0e474366540f385fe54057469d77e437de13f25186599b988c883fbc1cf3e2faee90a407e7c0ceab729099bf75354e8513b024040efd180308dc5f31f49543e9485ab6ec357a187151f8bc3eb37255576d5fa72e495aa4d28d280ecdd9dff0839d1fa5301b9e01f3442e034a933268f0bb2579d02403dac1f74b150e868265b8a53f47c8db1580459a60e771369bbcb83e507fde69ed189a4f238493911ec47eec8607378ca762edc7e13a7ebda54c75764ad4003f0";
	//智慧验证平台的公钥
	private static final String SMAU_PUB_KEY="30819f300d06092a864886f70d010101050003818d0030818902818100a136bd1399f94e46ff42eb6fad480621ca3e03c01e9fc8e1eab6d75dabc63e47ef1c2064fb10abaa491b9452e2d504de88223775e14931c6d8c7aab0f13d6673f567d7d0b0769539a749d920f50d7421f3fd66b5425bc3dee6f27b82de6243894e79b76a478efe79ff44ee813e789150871498186cbf1672a306469d250b703d0203010001";
	private static final String CONTENT_TYPE_TEXT = "application/json";
	public static void main(String[] args) throws Exception{
		String postMsg=getRequestMsg();
		
		CloseableHttpClient httpclient = HttpClients.custom().build();
        try {

            HttpPost httpPost = new HttpPost(POST_URL);
            
            System.out.println("Executing request " + httpPost.getRequestLine());
    		StringEntity se = new StringEntity(postMsg, "UTF-8");
    		se.setContentType(CONTENT_TYPE_TEXT);
    		se.setContentEncoding( "UTF-8");
    		httpPost.setEntity(se);
    	//	System.out.println("test"+httpPost.toString());
            CloseableHttpResponse response = httpclient.execute(httpPost);
            try {
                HttpEntity entity = response.getEntity();
                System.out.println(response.getStatusLine());
                if(200==(response.getStatusLine().getStatusCode())){
	                String respStr=IOUtils.toString(entity.getContent());
	                //进行报文签名的验证
	                System.out.println(respStr);
	                boolean result=PackAndUnpackDemo.verify(respStr, SMAU_PUB_KEY);
	                System.out.println("结果报文签名验证结果为:"+result);
                }
            } finally {
                response.close();
            }
        } finally {
            httpclient.close();
        }
	}
	
	public static String getRequestMsg() throws Exception{
		Map<String,String> body=new HashMap<String,String>();
//		body.put("cardNo", "6226580008234052");
//		body.put("cardNo", "6226580008232726");
		body.put("cardNo", "6222803761541110318");
//		body.put("cardNo", "6217920113292754");
		//String msg= PackAndUnpackDemo.getRequestMsg(body, CHANNEL_PRIVATE_KEY, "10008", "RISK001", "银行卡消费区间验证");
		//String msg= PackAndUnpackDemo.getRequestMsg(body, CHANNEL_PRIVATE_KEY, "10008", "RISK002", "银行卡受理习惯验证接口");
		String msg= PackAndUnpackDemo.getRequestMsg(body, CHANNEL_PRIVATE_KEY, "10008", "RISK003", "银行卡消费属地占比前三验证接口");
		System.out.println(msg);
		return msg;
	}
}
