package chinaums.common.util;

import com.alibaba.fastjson.annotation.JSONField;

/**
 * 2016/10/09 Json报文实体类
 * 
 * @author wuwenzhe
 * 
 */
public class OutputMessageProto {
	/**
	 * 报文头
	 */
	@JSONField(serialize = false)
	private MsgHeadProto msgHead;

	/**
	 * 报文体
	 */
	@JSONField(serialize = false)
	private Object msgBody;

	/**
	 * 报文签名
	 */
	@JSONField(serialize = false)
	private MsgMacProto msgMac;

	@JSONField(name = "MsgHead", ordinal = 0)
	public MsgHeadProto getMsgHead() {
		return this.msgHead;
	}

	@JSONField(name = "MsgHead")
	public void setMsgHead(MsgHeadProto msgHead) {
		this.msgHead = msgHead;
	}

	@JSONField(name = "MsgBody", ordinal = 1)
	public Object getMsgBody() {
		return this.msgBody;
	}

	@JSONField(name = "MsgBody")
	public void setMsgBody(Object msgBody) {
		this.msgBody = msgBody;
	}

	@JSONField(name = "MsgMac", ordinal = 2)
	public MsgMacProto getMsgMac() {
		return this.msgMac;
	}

	@JSONField(name = "MsgMac")
	public void setMsgMac(MsgMacProto msgMac) {
		this.msgMac = msgMac;
	}
}
