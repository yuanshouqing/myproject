package chinaums.common.util;

import java.io.Serializable;

public class Results implements Serializable {

	private static final long serialVersionUID = -9102048553222863997L;
	
	private String object_type;
	private String result;
	private String url_short;
	private String object_id;
	private String url_long;
	private String type;
	public String getObject_type() {
		return object_type;
	}
	public void setObject_type(String object_type) {
		this.object_type = object_type;
	}
	public String getResult() {
		return result;
	}
	public void setResult(String result) {
		this.result = result;
	}
	public String getUrl_short() {
		return url_short;
	}
	public void setUrl_short(String url_short) {
		this.url_short = url_short;
	}
	public String getObject_id() {
		return object_id;
	}
	public void setObject_id(String object_id) {
		this.object_id = object_id;
	}
	public String getUrl_long() {
		return url_long;
	}
	public void setUrl_long(String url_long) {
		this.url_long = url_long;
	}
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
}
