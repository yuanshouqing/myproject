package chinaums.dao;

import chinaums.entity.Test_Configure;

public interface Test_ConfigureDao {
    public Test_Configure selectConfigureByChannleId(String OrgSenderID);
}
