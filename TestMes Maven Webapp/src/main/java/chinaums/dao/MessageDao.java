package chinaums.dao;

import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Map;


public interface MessageDao {

    public List<Map<String,Object>>   getData();


}
